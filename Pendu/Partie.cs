﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pendu
{
    class Partie
    {
        public Joueur Joueur { get; private set; }
        public Mot Mot { get; private set; }
        public int NumeroTour { get; private set; }

        public Partie()
        {
            this.Joueur = new Joueur();
            this.Mot = new Mot();
            this.NumeroTour = 1;
            //On instancie le joueur et la partie, et on dit qu'on est au premier tour
        }

        public void Lancer()
        {
            while(this.Joueur.ADesVies())
            {
                this.Tour();
                Console.Clear();

                if (this.Mot.TotalementTrouve())
                    break;
                //Tant que le joueur a une/des vie(s) ou n'a pas gagné: On fait un tour
            }

            AfficherVictoireOuDefaite();
        }

        private void AfficherVictoireOuDefaite()
        {
            if (this.Joueur.ADesVies())
                Console.WriteLine($"Bravo {this.Joueur.Nom} ! Vous avez gagné, il vous reste {this.Joueur.NombreVies} vies ! ");
            else
                Console.WriteLine($"Dommage {this.Joueur.Nom}... Vous avez perdu, ne vous reste plus de vie...\nLe mot était : {this.Mot.MotADeviner}");
            //S'il reste des vies, il a gagné, sinon on lui dit qu'il a perdu et on affiche le mot
        }

        private void Tour()
        {
            Console.WriteLine($"Tour n°{this.NumeroTour}, vous avez {this.Joueur.NombreVies} vies\n");
            Console.WriteLine($"Mot : {this.Mot.ToString()}"); 
            //On affiche le numéro du tour et le nombre de vies du joueur
            //On affiche le mot (avec les étoiles et tout) (va voir "Mot.ToString()" car surchargé la méthode "ToString()"
            bool lettreCorrecte = this.Mot.ProposerLettre();
            //On va demander de proposer une lettre
            //Si la lettre était dans le mot on ne fait rien, si elle ne l'était pas, on enlève une vie
            if (!lettreCorrecte)
                this.Joueur.EnleverVie();

            this.NumeroTour++; //On augmente le numéro du tour.
        }
    }
}
