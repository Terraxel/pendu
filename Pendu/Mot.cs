﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Pendu
{
    class Mot
    {
        public string MotADeviner { get; private set; }
        public List<char> LettresProposees { get; private set; }

        public Mot()
        {
            Console.Write("Entrez le mot à deviner : ");
            this.MotADeviner = Console.ReadLine().ToLower();
            //ToLower() pour ne pas prendre en compte les majuscules
            Thread.Sleep(250);
            //Thread.Sleep me permet d'attendre X millisecondes (ici 250) pour éviter de faire la transition direct
            //Pour utiliser "Thread.Sleep" j'ai du inclure "System.Threading" avec "using System.Threading;" en en-tête
            Console.Clear();
            //On clear la console et on oublie pas d'instancier la liste (c'est un objet)
            this.LettresProposees = new List<char>();
        }

        public override string ToString()
        {
            string mot = null;

            foreach(var ch in MotADeviner)
            {
                if (LettresProposees.Contains(ch))
                    mot += ch;
                else
                    mot += "*";
                //Si on a déjà proposé la lettre, on la met dans le mot
                //Sinon on met une étoile (pour cacher la lettre à l'utilisateur)
            }

            return mot;
        }

        public bool TotalementTrouve()
        {
            foreach(char ch in this.MotADeviner)
            {
                if (!this.LettresProposees.Contains(ch))
                    return false;
            }
            //On parcourt chaque caractère du mot
            //Si on trouve que un caractère du mot n'a pas été proposé par le joueur, on sait qu'il n'a pas trouvé le mot
            //S'il n'y a aucun cas où le caractère n'a pas été trouvé, on finira la boucle et on renverra true (victoire)
            return true;
        }

        public bool ProposerLettre()
        {
            Console.Write("Proposez une lettre : ");
            char lettreProposee = Convert.ToChar(Console.ReadLine().ToLower());
            //char lettreProposee = char.Parse(Console.ReadLine().ToLower()); => Fonctionne aussi.
            this.LettresProposees.Add(lettreProposee);

            if (this.MotADeviner.Contains(lettreProposee))
                return true;
            else
                return false;

            //On récupère une lettre, on l'ajoute dans la liste des lettres que l'utilisateur à proposé.

            //On regarde si le mot contient cette lettre et on renvoie vrai ou faux selon le cas
            //Retourner vrai ou faux me permettra après de savoir si je dois enlever une vie ou pas.
        }
    }
}
