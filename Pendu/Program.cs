﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pendu
{
    class Program
    {
        static void Main(string[] args)
        {
            bool playAgain = false;

            do
            {
                Partie partie = new Partie();
                partie.Lancer();
                //Tant que la partie ne sera pas finie, on ira pas plus loin
                partie = null;
                playAgain = PlayAgain();
                //On remet la partie à null, au moins tout sera recrée si on recommence
                //On demande au joueur s'il veut rejouer
                Console.Clear();
            } while (playAgain);
        }

        static bool PlayAgain()
        {
            while(true)
            {
                Console.Write("Play Again ? (O/N) : ");
                string input = Console.ReadLine();

                if (input.ToLower() == "n") return false;
                if (input.ToLower() == "o") return true;
            };
        }
    }
}
