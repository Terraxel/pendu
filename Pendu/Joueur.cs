﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pendu
{
    class Joueur
    {
        public string Nom { get; private set; }
        public int NombreVies { get; private set; }

        public Joueur()
        {
            Console.Write("Entrez votre nom : ");
            this.Nom = Console.ReadLine();

            this.NombreVies = 5; //On met 5 vies par défaut
        }

        public bool ADesVies()
        {
            //La c'est explicite, pas besoin d'expliquer mdr
            if (this.NombreVies > 0)
                return true;
            else
                return false;
        }

        public void EnleverVie()
        {
            this.NombreVies--;
            //On enlève une vie en décrémentant le compteur
        }
    }
}
