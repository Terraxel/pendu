# Introduction

- On essaie d'imaginer chaque "chose" en tant qu'objet

# Classes

- On utilisera différentes classes pour dissocier les composants de notre programme

## Classe mot

- La classe mot nous servira à contenir le mot recherché, ainsi qu'une liste de caractères qui ont déjà été proposés pour ce mot.

- On surchargera "ToString()" afin de récupérer le mot avec seulement les caractères qui ont déjà été trouvés et des étoiles pour ceux ne l'ayant pas été :

```cs
        public override string ToString()
        {
            string mot = null;

            foreach(var ch in MotADeviner)
            {
                if (LettresProposees.Contains(ch))
                    mot += ch;
                else
                    mot += "*";
                //Si on a déjà proposé la lettre, on la met dans le mot
                //Sinon on met une étoile (pour cacher la lettre à l'utilisateur)
            }

            return mot;
        }
```

- On aura une méthode qui permettra de donner si oui ou non le mot a été totalement dévoilé :

        On parcourt chaque caractère du mot.

        Si on trouve que un caractère du mot n'a pas été proposé par le joueur, on sait qu'il n'a pas trouvé le mot.

        S'il n'y a aucun cas où le caractère n'a pas été trouvé, on finira la boucle et on renverra true (victoire).

- Et on aura une troisième méthode pour proposer une lettre pour le mot qui renverra "true" si la lettre est effectivement dans le mot et "false" si elle ne l'est pas.

## Classe Joueur

- Cette classe contiendra le nom du joueur et ses vies.

- On aura deux méthodes :

    - Une permettant d'enlever une vie au joueur
    - L'autre permettant de vérifier si le joueur possède des vies.


## Classe Partie

- Cette classe contiendra un joueur et un mot.

- On aura une méthode publique permettant de lancer partie.

- Plusieurs méthodes privées : 

    - Une méthode qui fait un tour :

    ```cs
        private void Tour()
        {
            Console.WriteLine($"Tour n°{this.NumeroTour}, vous avez {this.Joueur.NombreVies} vies\n");
            Console.WriteLine($"Mot : {this.Mot.ToString()}"); 
            //On affiche le numéro du tour et le nombre de vies du joueur
            //On affiche le mot (avec les étoiles et tout) (va voir "Mot.ToString()" car surchargé la méthode "ToString()"
            bool lettreCorrecte = this.Mot.ProposerLettre();
            //On va demander de proposer une lettre
            //Si la lettre était dans le mot on ne fait rien, si elle ne l'était pas, on enlève une vie
            if (!lettreCorrecte)
                this.Joueur.EnleverVie();

            this.NumeroTour++; //On augmente le numéro du tour.
        }
    ```
    => On écrit le numéro du tour avec le nombre de vies et le mot avec seulement les caractères trouvés. Le joueur propose une lettre, si elle n'est pas dans le mot, on enlève une vie. Et enfin, on indique que le tour est terminé en incrémentant la valeur du numéro du tour.

# Conclusion sur l'orienté-objet

## Lisibilité

- On rend le code très lisible si on nomme bien nos méthodes membres, etc...

- TO BE CONTINUED...